import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ButtonModule} from 'primeng/button';
import { InicioComponent } from './inicio/inicio.component';
import {FormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {CheckboxModule} from 'primeng/checkbox';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {TableModule} from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {KeyFilterModule} from 'primeng/keyfilter';
// import { InicioService } from './inicio/inicio.service';
import { HttpClientModule } from '@angular/common/http';
import { InicioService } from './inicio/inicio.service';
import {CalendarModule} from 'primeng/calendar';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {CardModule} from 'primeng/card';


@NgModule({
  declarations: [
    AppComponent,
    InicioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InputTextModule,
		CheckboxModule,
		ButtonModule,
    RadioButtonModule,
    CalendarModule,
		InputTextareaModule,
    DropdownModule,
    HttpClientModule,
    ToggleButtonModule,
    FormsModule,
    TableModule,
    BrowserAnimationsModule,
    KeyFilterModule,
    CardModule
  ],
  providers: [
     InicioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
