import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import { Alumno } from 'src/clases/alumno';
import { Book } from 'src/clases/book';
import { Grado } from 'src/clases/grado';
import { InicioService } from './inicio.service';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  lstBooks: Book[] = [];
  book: Book = new Book();

  cols: any[];



  constructor(private inicioService: InicioService) { }

  ngOnInit(): void {
    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'author', header: 'autor' },
      { field: 'name', header: 'nombre libro' },
      { field: 'price', header: 'precio' },
      { field: 'available', header: 'disponible' },
      { field: 'publicationDate', header: 'fecha' }
    ];

    this.getBooks();

  }


  Guardar() {
    this.inicioService.saveBook(this.book).subscribe((data) => {
      console.log(data)
    })
    this.limpiar();
    this.getBooks();
  }

  limpiar() {
    this.book = new Book();

  }


  getBooks() {
    this.inicioService.sendGetRequest().subscribe((data:any) => {
      this.lstBooks = data.value;
      console.log(data)

    })
  }

}
