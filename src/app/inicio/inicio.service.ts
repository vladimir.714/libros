import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from 'src/clases/book';

@Injectable()
export class InicioService {

  private REST_API_SERVER = "http://localhost:8090/api/library/book";

  constructor(private httpClient: HttpClient) { }


  public sendGetRequest(){
    return this.httpClient.get(this.REST_API_SERVER+"/all");
  }


  public saveBook(book:Book){
    return this.httpClient.post(this.REST_API_SERVER+"/save",book);
  }
}
