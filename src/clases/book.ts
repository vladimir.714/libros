export class Book{
  author?: string
  available?: boolean
  id?: number
  name?: string
  price?: number
  publicationDate?: Date
}
